﻿using MMLib.DataBase;
using NLog;
using System;
using System.Data;
using System.Windows.Forms;
using static MMLib.DataStorage;

namespace LDBParser.Data
{
    public class AbstractDataStrategy : IDataStrategy
    {
        internal static Logger _logger = LogManager.GetCurrentClassLogger();
        internal LocalDataBase LDB = LocalDataBase.Instance;
        internal TypeCollection _typeTable;

        public AbstractDataStrategy(TypeCollection typeCollection)
        {
            _typeTable = typeCollection;
        }

        public virtual void DeleteData(object data)
        {
            try
            {
                var blockRows = (DataGridViewSelectedRowCollection)data;
                foreach (DataGridViewRow row in blockRows)
                {
                    var selectedDrillBlockID = Convert.ToInt32(row.Cells["ID"].Value);
                    LDB.DeleteItem(_typeTable, selectedDrillBlockID);
                }

            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

        }

        public virtual void InsertData(object data)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable LoadData()
        {
            throw new NotImplementedException();
        }

        public virtual void ReplaceData(object data)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateData(object data)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateRecord(object data)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteRecord(object data)
        {
            throw new NotImplementedException();
        }
    }
}
