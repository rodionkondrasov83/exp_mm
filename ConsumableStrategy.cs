﻿using System;
using MMLib.Objects;
using System;

using System.Data;
using System.Windows.Forms;

namespace LDBParser.Data
{
    class ConsumableStrategy : AbstractDataStrategy
    {
        public ConsumableStrategy() : base(MMLib.DataStorage.TypeCollection.Consumable) { }

        public override DataTable LoadData()
        {
            return LDB.ExecuteQuery("SELECT ID AS Код, NAME AS Имя, TYPE_ID AS [Код типа], NUM_METERS_DRILLED AS [Количество пробуренных метров], ACTIVE AS Активно FROM CONSUMABLE");
        }
        public override void UpdateRecord(object data)
        {
            try
            {
                var operatorRow = (DataGridViewRow)data;
                var id = Convert.ToInt32(operatorRow.Cells["Код"].Value);
                var name = operatorRow.Cells["Имя"].Value.ToString();
                var typeId = Convert.ToInt32(operatorRow.Cells["Код типа"].Value);
                var unitWeig = Convert.ToSingle(operatorRow.Cells["Количество пробуренных метров"].Value);
                var active = Convert.ToBoolean(operatorRow.Cells["Активно"].FormattedValue);
                Consumable consumable = new Consumable(id, name, typeId);
                LDB.CreateOrUpdateConsumable(consumable);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        public override void ReplaceData(object data)
        {
            throw new NotImplementedException();
        }

        public override void UpdateData(object data)
        {
            throw new NotImplementedException();
        }
        public override void DeleteRecord(object data)
        {
            try
            {
                var blockRow = (DataGridViewRow)data;
                var id = Convert.ToInt32(blockRow.Cells["Код"].Value);
                LDB.DeleteItem(_typeTable, id);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }
}
