﻿using LDBParser.Data;
using System;
using System.Collections.Generic;
using System.Data;

namespace LDBParser
{
    public static class DataUtils
    {
        private static IDataStrategy _dataStrategy;
        public static void SetDataStrategy(Tables tables)
        {
            switch (tables)
            {
                case Tables.Blocks:
                    _dataStrategy = new DrillBlockStrategy(); 
                    break;
                case Tables.Operators:
                    _dataStrategy = new OperatorStrategy();
                    break;
                case Tables.UnitDrill:
                    _dataStrategy = new UnitDrillStrategy();
                    break;
                case Tables.Unit:
                    _dataStrategy = new UnitStrategy();
                    break;
                case Tables.Consumable:
                    _dataStrategy = new ConsumableStrategy();
                    break;
                case Tables.Material:
                    _dataStrategy = new MaterialStrategy();
                    break;
                case Tables.Status:
                    _dataStrategy = new StatusStrategy();
                    break;
            }

        }

        public static void DeleteData(object data)
        {
            _dataStrategy.DeleteData(data);
        }

        public static DataTable LoadData()
        {
            return _dataStrategy.LoadData();
        }

        public static void InsertData(object data)
        {
            _dataStrategy.InsertData(data);
        }

        public static void UpdateRecord(object data)
        {
            _dataStrategy.UpdateRecord(data);
        }

        public static void DeleteRecord(object data)
        {
            _dataStrategy.DeleteRecord(data);
        }
    }
}