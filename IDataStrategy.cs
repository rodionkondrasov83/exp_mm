﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace LDBParser.Data
{
    interface IDataStrategy
    {
        DataTable LoadData();
        void InsertData(object data);
        void DeleteData(object data);
        void UpdateData(object data);
        void ReplaceData(object data);
        void UpdateRecord(object data);
        void DeleteRecord(object data);
    }
}
