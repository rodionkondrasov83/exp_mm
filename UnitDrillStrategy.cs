﻿using MMLib.DataBase;
using MMLib.Objects;

using System;
using System.Data;
using System.Windows.Forms;

namespace LDBParser.Data
{
    class UnitDrillStrategy : AbstractDataStrategy
    {
        public UnitDrillStrategy() : base(MMLib.DataStorage.TypeCollection.Unkown) { }
        public override void InsertData(object data)
        {
            throw new NotImplementedException();
        }

        public override DataTable LoadData()
        {
            var query = "SELECT UNIT_ID AS [Код юнита] , HIGH_PRECISION AS [Высокая точность], WIDTH AS Ширина, LENGTH AS Длина FROM UNIT_DRILL";
            return LDB.ExecuteQuery(query);
        }

        public override void DeleteData(object data)
        {
            try
            {
                var unitRows = (DataGridViewSelectedRowCollection)data;
                foreach (DataGridViewRow row in unitRows)
                {
                    var selectedDrillUnitId = Convert.ToInt32(row.Cells["Код юнита"].Value);
                    LDB.DeleteDrillUnit(selectedDrillUnitId);
                }

            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        public override void ReplaceData(object data)
        {
            throw new NotImplementedException();
        }
        public override void UpdateData(object data)
        {
            throw new NotImplementedException();
        }
        public override void UpdateRecord(object data)
        {           
            try
            {
                var unitRow = (DataGridViewRow) data;
                var id = Convert.ToInt32(unitRow.Cells["Код юнита"].Value);
                var highPrecision = Convert.ToBoolean(unitRow.Cells["Высокая точность"].FormattedValue);
                var width = Convert.ToDouble(unitRow.Cells["Ширина"].Value);
                var length = Convert.ToDouble(unitRow.Cells["Длина"].Value);
                LDB.CreateOrUpdateUnitDrill(id, highPrecision, width, length);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        public override void DeleteRecord(object data)
        {
            try
            {
                var unitRow = (DataGridViewRow) data;
                var id = Convert.ToInt32(unitRow.Cells["Код юнита"].Value);
                LDB.DeleteDrillUnit(id);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }
}
