﻿using MMLib.DataBase;
using MMLib.Objects;

using System;
using System.Data;
using System.Windows.Forms;

namespace LDBParser.Data
{
    public class UnitStrategy : AbstractDataStrategy
    {
        public UnitStrategy() : base(MMLib.DataStorage.TypeCollection.Unit) { }

        public override void InsertData(object data)
        {
            throw new NotImplementedException();
        }

        public override DataTable LoadData()
        {
            var query = "SELECT ID AS Код, NAME AS Имя, AREA_ID AS [Код зоны] FROM UNIT";
            return LDB.ExecuteQuery(query);
        }

        public override void ReplaceData(object data)
        {
            throw new NotImplementedException();
        }

        public override void UpdateData(object data)
        {
            try
            {
                var blockRows = (DataGridViewSelectedRowCollection)data;
                foreach (DataGridViewRow row in blockRows)
                {
                    var id = Convert.ToInt32(row.Cells["Код"].Value);
                    var name = row.Cells["Имя"].Value.ToString();
                    var areaId = Convert.ToInt32(row.Cells["Код зоны"].Value);
                   // var typeId = Convert.ToInt32(row.Cells["Код типа"].Value);

                    Unit unit = new Unit(id, name, 1, areaId);
                    LDB.CreateOrUpdateUnit(unit);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public override void UpdateRecord(object data)
        {
            try
            {
                var row = (DataGridViewRow)data;

                var id = Convert.ToInt32(row.Cells["Код"].Value);
                var name = row.Cells["Имя"].Value.ToString();
                var areaId = Convert.ToInt32(row.Cells["Код зоны"].Value);
                //var typeId = Convert.ToInt32(row.Cells["Код типа"].Value);

                Unit unit = new Unit(id, name, 1, areaId);
                LDB.CreateOrUpdateUnit(unit);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public override void DeleteRecord(object data)
        {
            try
            {
                var unitRow = (DataGridViewRow)data;
                var id = Convert.ToInt32(unitRow.Cells["Код"].Value);
                LDB.DeleteItem(_typeTable, id);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }
}
